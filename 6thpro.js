// Create a javascript object to store your name, age, date of birth, mother's name and father's name.

// taking a variable
let mySelf = {
    name : 'siddhesh',
    age : 21,
    DOB : "30 July 2001",
    mothersName : 'Suman redkar',
    fathersName : 'Shanataram redkar'
}
// using for loop

for(let key in mySelf)
{
    console.log(`My ${key} is ${mySelf[key]}`);
}