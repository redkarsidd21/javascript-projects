// program to find the factorial of a number

function fact(n){
    // using if-else

    if(n==0)
      return 1;
    else
      return n * fact(n-1);
}
let num = 5;
let result =fact(num);

console.log(result);