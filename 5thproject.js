// if-else 
let age = 12;
if (age > 18) {
    console.log("able to open bank account");
} else {
    console.log("not able to open bank account");
}

// using switch statement
// taking a variable
let a = 2;

switch (a) {

    case 1:
        a = 'one';
        break;
    case 2:
        a = 'two';
        break;
}
console.log(`The value is ${a}`);

// template literals

// Without template literal
console.log('learning web development \nin NCT!');
  
// With template literal
console.log(`learning web development
in NCT!`);