// Given a string of multiple words, write javascript code to reverse each word of the string.


function ReverseString(str) {
 
    // Returning reverse string
return [...str].reduce((x, y) => y.concat(x));
}

console.log(ReverseString("siddhesh"));
console.log(ReverseString("redkar"));