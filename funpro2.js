// Create a javascript function check_first_capital() that takes as input a string and checks if first letter of the string is in upper case.

function firstIsUppercase(str)
 {
    if (typeof str !== 'string' || str.length === 0)
     {
      return false;
    }
  
    if (str[0].toUpperCase() === str[0])
     {
      return true;
    }
  
    return false;
  }
  
  console.log(firstIsUppercase('Siddhesh')); 
  console.log(firstIsUppercase('redkar')); 
  
  if (firstIsUppercase('Siddhesh')) 
  {

  } 
  else
   {
    console.log('First letter is NOT uppercase')
  }